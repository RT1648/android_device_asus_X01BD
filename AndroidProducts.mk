#
# Copyright (C) 2020 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/colt_X01BD.mk

COMMON_LUNCH_CHOICES := \
    colt_X01BD-eng \
    colt_X01BD-user \
    colt_X01BD-userdebug
